module:
	gcc -pipe -Wall -Wstrict-prototypes -Wmissing-prototypes -Wmissing-declarations -g3 -Iinclude -I/usr/include -D_REENTRANT -D_GNU_SOURCE -O6  -fomit-frame-pointer -Wno-missing-prototypes -Wno-missing-declarations -DCRYPTO -fPIC -c -o func_alotech_dst_chan.o func_alotech_dst_chan.c
	gcc func_alotech_dst_chan.o -shared -o func_alotech_dst_chan.so

install:
	cp func_alotech_dst_chan.so /usr/lib/asterisk/modules
clean:
	rm func_alotech_dst_chan.o func_alotech_dst_chan.so

